const path = require( 'path' );
const webpack = require( 'webpack' );

module.exports = {
	entry: {
		"tag-manager": './src/js/tag-manager.js',
	},
	output: {
		filename: '[name].js',
		path: path.resolve( __dirname, 'build/js' ),
	},
	module: {
		rules: [
			{
			test: /\.(js)$/,
			exclude: /node_modules/,
			use: ['babel-loader']
		}
		]
	},
	resolve: {
	extensions: ['*', '.js']
	},
};
