<?php

namespace Superskrypt\WpTagManager;

class WpManagerTexts {
	
    const DEFAULT_COOKIES_MANAGER_CONSENT_TEXTS = array(
		'pl' => array(
			'headingText'		=> "Polityka prywatności",
			'content' 		    => 'Zbieramy Twoje dane, anonimizujemy je i wykorzystujemy w celach funkcjonalnych i marketingowych. Dzięki temu możemy tworzyć lepszą stronę internetową.',
			'afterContent' 		=> '[PRIVACY_LINK label="Chcę wiedzieć więcej"]',
			'acceptButtonLabel' => 'Akceptuję',
            "rejectButtonLabel" => 'Odrzuć',
			'categoriesNames'	=> ['koniecznych', 'analitycznych', 'marketingowych'],
		),
		'en' => array(
			'headingText'		=> 'Your Privacy',
			'content'			=> '<p>We collect your data, anonymise it and use it for functional and marketing purposes.<br>This lets us create a better website.</p>',
			"afterContent"      => '[PRIVACY_LINK]Privacy Policy[/PRIVACY_LINK]',
			'acceptButtonLabel' => "I’m ok with this",
			"rejectButtonLabel" => 'Reject',
			'categoriesNames'	=> ['necessary', 'analytic', 'marketing'],
		),
		'ru' => array(
			'headingText'		=> "политика конфиденциальности",
			'content' 		    => 'Мы используем файлы «cookie»',
			'acceptButtonLabel' => 'Я принимаю',
			'afterContent' 		=> '[PRIVACY_LINK label="Я хочу узнать больше"]',
            "rejectButtonLabel" => 'Reject',
			'categoriesNames'	=> ['необходимые', 'аналитические', 'маркетинговые'],
		),
	);

	const DEFAULT_COOKIES_MANAGER_NOTICE_TEXTS = array(
		'pl' => array(
			'headingText'		=> "",
			'content' 		    => 'Używamy plików cookies w celu poprawy jakości serwisu. Przetwarzamy jedynie dane zanonimizowane.',
			'afterContent' 		=> '[PRIVACY_LINK label="Chcę wiedzieć więcej"]',
            'acceptButtonLabel' => 'Rozumiem',
		),
		'en' => array(
			'headingText'		=> "",
			'content' 		    => 'We use cookies.',
			'afterContent' 		=> '[PRIVACY_LINK label="I want to know more"]',
			'acceptButtonLabel' => 'I understand',
		),
		'ru' => array(
			'headingText'		=> "",
			'content' 	        => 'Наш веб-сайт использует файлы cookie.',
            'afterContent' 		=> '[PRIVACY_LINK label="Я хочу знать больше"]',
			'acceptButtonLabel' => 'Я понимаю',
		),
	);

	const DEFAULT_CONSENT_BOX_TEXTS = [
		'consent' => self::DEFAULT_COOKIES_MANAGER_CONSENT_TEXTS,
		'notice' => self::DEFAULT_COOKIES_MANAGER_NOTICE_TEXTS,
	];

    public static function getProcessedTextsForLanguage($textsArray) {
        return array_map(function ( $texts) {
                if(is_string($texts)) {
                    return do_shortcode($texts);
                }
                return $texts;
            }, $textsArray);
    }

    public static function get_cookies_manager_texts($mode = 'consent') {
		$texts = array();
		$languages = self::getSiteLanguages();
		foreach ( $languages as $lang ) {
			$texts[$lang['code']] = self::getConsentBoxText($lang, $mode);
		}
		return $texts;
	}

	public static function getSiteLanguages() {
		$site_lang = defined('SITE_LANGS') 
								? SITE_LANGS 
								: array(
									'pl' => array(
										'code' => 'pl',
									),
							);
		$languages = function_exists( 'icl_get_languages' ) ? (!empty(icl_get_languages('skip_missing=0&orderby=code')) ? icl_get_languages('skip_missing=0&orderby=code') : $site_lang) : $site_lang;
		return $languages;
	}

	public static function getCurrentLanguage() {
		if(has_filter('wpml_current_language') && !empty(apply_filters( 'wpml_current_language', NULL ))) {
			return apply_filters( 'wpml_current_language', NULL );
		}
		$site_langs = defined('SITE_LANGS') 
								? SITE_LANGS 
								: array(
									'pl' => array(
										'code' => 'pl',
									),
								);
		$firstLang = reset($site_langs);
		return isset($firstLang['code']) ? $firstLang['code'] : "";

	}

	/**
	 * @param String $lang kod języka
	 * @param consent|notice $consentBoxType tryb consent boxa. Dopuszczalne wartości "consent lub "notice"
	 */
	public static function getConsentBoxText($lang, $consentBoxType = 'consent') {
		$defaultTexts = self::DEFAULT_CONSENT_BOX_TEXTS[$consentBoxType] ? self::DEFAULT_CONSENT_BOX_TEXTS[$consentBoxType] : [];
		if ( defined('COOKIES_MANAGER_TEXTS') && isset(COOKIES_MANAGER_TEXTS[$lang['code']]) ) {
			$text = self::getProcessedTextsForLanguage(COOKIES_MANAGER_TEXTS[$lang['code']]);
		} 
		elseif (isset($defaultTexts[$lang['code']])) {
			$defaultText = $defaultTexts[$lang['code']];
			$text = self::getProcessedTextsForLanguage($defaultText);
		}
		else {
			$firstLang = defined('COOKIES_MANAGER_TEXTS') ? array_key_first(COOKIES_MANAGER_TEXTS) : array_key_first($defaultTexts);
			$text = defined('COOKIES_MANAGER_TEXTS') && isset(COOKIES_MANAGER_TEXTS[$firstLang]) ? self::getProcessedTextsForLanguage(COOKIES_MANAGER_TEXTS[$firstLang]) : self::getProcessedTextsForLanguage($defaultTexts[$firstLang]) ;
		}
		return $text;
	}	
}
