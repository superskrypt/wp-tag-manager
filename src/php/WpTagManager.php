<?php
namespace Superskrypt\WpTagManager;
use \Superskrypt\WpTagManager\PrivacyPolicyShortcode;
use \Superskrypt\WpTagManager\WpManagerTexts;
class WpTagManager {
    public static function init() {
        WpTagManager::cookiesSetup();
		CookieConsentShortcode::setup();
        PrivacyPolicyShortcode::setup();
    
    }

	public static function cookiesSetup() {
		add_action( 'wp_enqueue_scripts', array(__CLASS__, 'enqueue_cookies_manager') );
	}

	public static function enqueue_cookies_manager() {
		wp_enqueue_script( 'tag-manager',  WpTagManager::getPackageJsPath('tag-manager.js'), array(), rand(), true);
		if (!(defined('COOKIES_MANAGER_NOTICE') && COOKIES_MANAGER_NOTICE)) {
			$params = array(
				'type' => 'consent',
                'reject' => WpTagManager::isRejectOptionEnabled(),
				'texts' => WpManagerTexts::get_cookies_manager_texts('consent'),
				'defaults' => WpTagManager::get_cookies_manager_consent_defaults(),
                'enable_user_inputs' => (defined('COOKIES_MANAGER_DISABLE_USER_INPUTS') &&  COOKIES_MANAGER_DISABLE_USER_INPUTS) ? false : true,
			);
		} else {
			$params = array(
				'type' => 'notice',
                'reject' => WpTagManager::isRejectOptionEnabled(),
				'texts' => WpManagerTexts::get_cookies_manager_texts('notice'),
				'defaults' => WpTagManager::get_cookies_manager_notice_defaults(),
                'enable_user_inputs' => false,
			);
		}
		if (defined("GOOGLE_ANALITICS_ID")) {
			$params['GA'] = array(
				'id' => GOOGLE_ANALITICS_ID,
			);
		}
        if (defined("FACEBOOK_PIXEL_ID")) {
			$params['FB'] = array(
				'id' => FACEBOOK_PIXEL_ID,
			);
		}
        if (defined("ANALYTIC_GTM_CONTAINER_ID")) {
			$params['GTM_A'] = array(
				'id' => ANALYTIC_GTM_CONTAINER_ID,
			);
		}
        if (defined("MARKETING_GTM_CONTAINER_ID")) {
			$params['GTM_M'] = array(
				'id' => MARKETING_GTM_CONTAINER_ID,
			);
		}
        if (defined("HOTJAR_SITE_ID")) {
			$params['HJ'] = array(
				'id' => HOTJAR_SITE_ID,
			);
		}
		// TODO configure FB PIX
		wp_localize_script( 'tag-manager', 'CookiesManagerParams', $params );
	}

    public static function isRejectOptionEnabled() {
        return (defined('COOKIES_MANAGER_REJECT_MODE') && COOKIES_MANAGER_REJECT_MODE) ? true : false;
    }

	public static function get_cookies_manager_consent_defaults() {
		return array(
			'analytic' => false,
			'marketing' => false,
		);
	}

	public static function get_cookies_manager_notice_defaults() {
		return array(
			'analytic' => true,
			'marketing' => true,
		);
	}
	public static function getPackageSrcPath() {
		return get_template_directory_uri() . '/../vendor/superskrypt/wp-tag-manager/build/';
	}

	public static function getPackageJsPath($file) {
		$dir = WpTagManager::getPackageSrcPath() . 'js';
        if ($file) {
            $dir = "$dir/$file";
        }
        return $dir;
	}
}
