<?php

namespace Superskrypt\WpTagManager;

class PrivacyPolicyShortcode {

    public static function setup() {
        add_shortcode( 'PRIVACY_LINK', array(get_called_class(), 'privacyPolicyLinkShortcode') );
    }

    public static function privacyPolicyLinkShortcode($atts, $content = null) {
        $params = shortcode_atts( array(
            'label' => 'Privacy policy',
            'link' => get_privacy_policy_url()
        ), $atts );

        $html = "";
        $html .= '<a class="privacy-policy-link" href="' . $params['link'] . '" > ';
        $html .= $content ? $content : $params['label'];
        $html .= '</a>';
        return $html;
    }

}
