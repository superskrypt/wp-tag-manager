<?php

namespace Superskrypt\WpTagManager;

define('PRIVACY_POLICY_CONSENT_SETTINGS_TEXTS', array(
	'pl' => array(
		'header' 			=> 'Ustawienia ciasteczek w Twojej przeglądarce',
		'categoriesNames'	=> ['niezbędne pliki cookies', 'cookies analityczne', 'cookies marketingowe'],
		'button' 			=> 'Zapisz ustawienia',
	),
	'en' => array(
		'header' 			=> 'Cookies settings in your browser',
		'categoriesNames'	=> ['necessary cookies', 'analytic cookies', 'marketing cookies'],
		'button' 			=> 'Save preferences',
	),
	'ru' => array(
		'header' 			=> 'Мы используем необходимые/аналитические/маркетинговые файлы',
		'categoriesNames'	=> ['necessary cookies', 'analytic cookies', 'marketing cookies'],
		'button' 			=> 'сохранить настройки',
	),
));

class CookieConsentShortcode {

	static public function getTexts() {
		return defined('CUSTOM_PRIVACY_POLICY_CONSENT_SETTINGS_TEXTS') ? CUSTOM_PRIVACY_POLICY_CONSENT_SETTINGS_TEXTS : PRIVACY_POLICY_CONSENT_SETTINGS_TEXTS;
	}
    public static function setup() {
		add_shortcode( 'cookies_settings', function($atts) {

			$atts = shortcode_atts( array(
				'lang' => "",
			), $atts );

			$language_code = !empty($atts['lang']) ? $atts['lang'] : WpManagerTexts::getCurrentLanguage(); //apply_filters( 'wpml_current_language', NULL );
			$texts = self::getTexts();
			$current_lang_settings = $texts[$language_code]; //${"cookies_settings_" . $language_code};
			$cookies_categories = ['necessary', 'analytic', 'marketing'];
			$checkboxes = '';

			for ($i = 0; $i < count($cookies_categories); $i++) {
				$checkStatus = $cookies_categories[$i] === 'necessary' ? ' disabled checked' : ' checked';
                $name = $current_lang_settings['categoriesNames'][$i];
                $cookieId = $cookies_categories[$i] . '-cookies-consent';
				$checkboxes .= "<div class='input-group'>
					<input id='$cookieId' class='cookies-category' name='$cookieId' type='checkbox' $checkStatus>
					<label for='$cookieId' class='cookies-category__label'>
						$name
					</label>
				</div>";
			};

			return "<div class='cookies-settings-box'>
						<h2 class='cookies-setting-header'> $current_lang_settings[header] </h2>
						<form method='post' class='cookies-categories-form'>
							$checkboxes
							<button type='submit' class='save-settings-button change-cookies'>$current_lang_settings[button]</button>
						</form>
					</div>";
		} );
	}
}
