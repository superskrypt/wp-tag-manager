export const necessaryStorageName = 'necessary-cookies';
export const analyticStorageName = 'analytic-cookies';
export const marketingStorageName = 'marketing-cookies';
export const localStorageNameSuffix = '-consent';

export default class CookiesConsentRepository {
    static getConsentNecessary() {
        return CookiesConsentRepository.getConsent(necessaryStorageName);
    }

    static getConsentAnalytic() {
        return CookiesConsentRepository.getConsent(analyticStorageName);
    }

    static getConsentMarketing() {
        return CookiesConsentRepository.getConsent(marketingStorageName);
    }

    static getConsent(name) {
        return localStorage.getItem(name + localStorageNameSuffix) == 'true';
    }

    static saveConsentNecessary(value) {
        return CookiesConsentRepository.saveConsent(necessaryStorageName, value);
    }

    static saveConsentAnalytic(value) {
        return CookiesConsentRepository.saveConsent(analyticStorageName, value);
    }

    static saveConsentMarketing(value) {
        return CookiesConsentRepository.saveConsent(marketingStorageName, value);
    }

    static saveConsent(name, value) {
        return localStorage.setItem(name + localStorageNameSuffix, value);
    }

    static getUserConsents() {
        return {
            necessary: CookiesConsentRepository.getConsentNecessary(),
            analytic: CookiesConsentRepository.getConsentAnalytic(),
            marketing: CookiesConsentRepository.getConsentMarketing(),
        }
    }

    static saveUserConsents(c) {
        CookiesConsentRepository.saveConsentNecessary(c.necessary);
        CookiesConsentRepository.saveConsentAnalytic(c.analytic);
        CookiesConsentRepository.saveConsentMarketing(c.marketing);
    }

    static consentsExistInLocalStorage() {
        return localStorage.getItem(`${necessaryStorageName}${localStorageNameSuffix}`) !== null
                ||
                localStorage.getItem(`${analyticStorageName}${localStorageNameSuffix}`) !== null
                ||
                localStorage.getItem(`${marketingStorageName}${localStorageNameSuffix}`) !== null 
    }
}
