import CCR from "./CookiesConsentRepository";

export default class PrivacyPolicyConsents {
    constructor() {

    }

    init() {
        const cookiesBox = document.querySelector('.cookies-settings-box');
        if (cookiesBox) {
            const preferencesForm = cookiesBox.querySelector('.cookies-categories-form');
            const checkboxes = cookiesBox.querySelectorAll('.cookies-category');
            const userContents = CCR.getUserConsents();
            checkboxes.forEach((checkbox) => {
                const consentName = checkbox.getAttribute('name');
                const userConsentCategory = Object.keys(userContents)
                    .filter((consentCategory => {
                        return consentName.search(consentCategory) > -1;
                    }))
                    .shift();
                if(userConsentCategory && CCR.consentsExistInLocalStorage()) {
                    checkbox.checked = userContents[`${userConsentCategory}`];
                }
                else {
                    checkbox.checked = true;
                }
            });
            preferencesForm.addEventListener('submit', this.onUpdatePreferences.bind(this));
        }
    }

    onUpdatePreferences(e) {
        e.preventDefault();
        const consents = {
            'necessary': e.target.elements['necessary-cookies-consent'].checked,
            'analytic': e.target.elements['analytic-cookies-consent'].checked,
            'marketing': e.target.elements['marketing-cookies-consent'].checked
        }
        CCR.saveUserConsents(consents);
    }

    static autoUpdateCheckboxesOnPrivacyPolicy() {
        const cookiesBox = document.querySelector('.cookies-settings-box');
        if (cookiesBox) {
            const checkboxes = cookiesBox.querySelectorAll('.cookies-category');
            checkboxes.forEach((checkbox) => {
                let consentName = checkbox.getAttribute('name');
                if (CCR.getConsent(consentName)) {
                    checkbox.checked = CCR.getConsent(consentName);
                }
                else {
                    checkbox.checked = false;
                }
            });
        }
    }
}