import CookiesBox from "./CookiesBox";
import CookiesConsentRepository from "./CookiesConsentRepository";
import PrivacyPolicyConsents from "./PrivacyPolicyConsents";
import FbPixelTag from "./tags/FbPixelTag";
import GaTag from "./tags/GaTag";
import GtmTag from "./tags/GtmTag";
import HotjarTag from "./tags/HotjarTag";

export default class TagManager {
    static init(tagManagerParams) {
        let userConsents = CookiesConsentRepository.getUserConsents();
        if ((!userConsents || !userConsents.necessary)) {
            const defaults = tagManagerParams.defaults;

            userConsents = TagManager.defaultConsents(tagManagerParams.type, defaults);
            new CookiesBox(tagManagerParams, consents => TagManager.setupTags(tagManagerParams, consents));
        }
        TagManager.setupTags(tagManagerParams, userConsents);
        new PrivacyPolicyConsents().init();
    }
    
    static defaultConsents(tagManagerType, defaults) {
        if (tagManagerType == 'notice') {
            return defaults;
            } 
        else {
            return { necessary: true };
        }
    }

    static setupTags(params, consents) {
        let tags = [GaTag, FbPixelTag, GtmTag, HotjarTag];
        tags.forEach(tag => tag.checkAndActivate(params, consents));
    }
}
