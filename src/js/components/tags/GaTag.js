let cachedAnonymisation;
export default class GaTag {

    static checkAndActivate(params, consents) {
        let  enableAnonymisation  = !consents.analytic;

        if (params.GA && params.GA.id && cachedAnonymisation !== enableAnonymisation ) {
            GaTag.activate(params.GA.id, enableAnonymisation );
            cachedAnonymisation = enableAnonymisation;
        }
    }

    static activate(scriptId, anonymisationEnabled) {
        return (function(w, d, s, l, i) {
            w[l] = w[l] || [];
            var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s),
                dl = l != 'dataLayer' ? '&l=' + l : '';
            j.async = true;
            j.src =
                'https://www.googletagmanager.com/gtag/js?id=' + i + dl;
            f.parentNode.insertBefore(j, f);
            j = d.querySelector('#GaTag');
            if (j == null){
                j = d.createElement(s);
                j.id = 'GaTag';
            }else{
                j.innerHTML = '';
            }
            j.innerHTML += 'window.dataLayer = window.dataLayer || [];\r\n';
            j.innerHTML += 'function gtag(){dataLayer.push(arguments);}\r\n';
            j.innerHTML += "gtag('js', new Date());\r\n";
            j.innerHTML += `gtag('config', '${scriptId}', { 'anonymize_ip': ${anonymisationEnabled} });\r\n`;
            f.parentNode.insertBefore(j, f);
        })(window, document, 'script', 'dataLayer', scriptId);
    }
}
