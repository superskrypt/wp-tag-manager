import { TemplateRenderer } from "@superskrypt/wp-theme-base";
import CCR, { necessaryStorageName, analyticStorageName, marketingStorageName } from "./CookiesConsentRepository";
import PrivacyPolicyConsents from "./PrivacyPolicyConsents";

export default class CookiesBox {
    constructor(cookiesManagerParams, onAcceptCallback) {
        this.cookiesManagerParams = cookiesManagerParams;
        this.setup();
    }

    setup() {
        switch (this.cookiesManagerParams.type) {
            case 'consent':
                this.createCookiesBox(['necessary', 'analytic', 'marketing'], true);
                break;
            case 'notice':
                this.createCookiesBox();
                this.saveDefaultCookiesConsents();
                break;
        }
    }

    setAcceptButtonHandler() {
        if(this.cookiesManagerParams.type == 'consent') {
            this.acceptButton.addEventListener('click', e => { e.preventDefault(); this.acceptCookies(onAcceptCallback); });
        }
        else {
            this.acceptButton.addEventListener('click',this.onAcceptNoticeHandler.bind(this));
        }
    }

    setRejectButtonHandler() {
        if(this.rejectButton) {
            this.rejectButton.addEventListener('click', (e) => { e.preventDefault(); this.onRejectClickHandler(); })
        }
    }

    createCookiesBox(categories = [], defaultChecked = false) {
        let CookiesTexts = this.getTextsForCurrentLanguage();

        this.consentBox = document.createElement("div");
        this.consentBox.classList.add('data-consent-box');
        this.consentBox.setAttribute('tabindex', '0');
        this.consentBox.setAttribute('role', 'dialog');
        this.consentBox.setAttribute('aria-modal', 'false');
        this.consentBox.setAttribute('aria-labelledby', 'data-consent-box-heading');
        this.consentBox.setAttribute('aria-describedby', 'data-consent-box-content');

        /* HEADING */
        if(CookiesTexts.hasOwnProperty('headingText') && CookiesTexts.headingText.length > 0) {
            const heading = document.createElement("div");
            heading.innerText = CookiesTexts.headingText;
            heading.classList.add('data-consent-box__heading');
            this.consentBox.appendChild( heading );
        }

        /* CONTENT */
        if(CookiesTexts.hasOwnProperty('content') && CookiesTexts.content.length > 0) {
            const content = document.createElement("div");
            content.classList.add('data-consent-box__content', 'consent-info')
            content.innerHTML = CookiesTexts.content;

            if(CookiesTexts.hasOwnProperty('afterContent') && CookiesTexts.afterContent.length > 0) {
                const afterContent = document.createElement("div");
                afterContent.classList.add('consent-info__after')
                afterContent.innerHTML = CookiesTexts.afterContent;
                content.appendChild(  afterContent );
            }
            this.consentBox.appendChild( content );
        }

        /* CONSENT FORM */
        if(this.cookiesManagerParams.type == 'consent' ) {
            this.consentsForm = this.createConsentsForm(categories, CookiesTexts, defaultChecked);
            this.consentBox.appendChild(this.consentsForm);
        }
        
        /* BUTTONS */
        this.consentBoxButtons = document.createElement("div");
        this.consentBoxButtons.classList.add('data-consent-box__buttons');

        new Promise(resolve => {
            const interval = setInterval(() => {
                if (window.TemplateRenderer) {
                    clearInterval(interval);
                    resolve();
                } else {
                }
            }, 250);
        })
        .then(() => {
            /* REJECT BUTTON */
            if(this.cookiesManagerParams.reject) {
                TemplateRenderer.getTemplate('button-secondary')
                .then((template) => {
                    template = template.replace('LABEL', CookiesTexts.rejectButtonLabel);
                    this.rejectButton = TemplateRenderer.createElementFromTemplate(template);
                    this.rejectButton.classList.add( 'data-consent-box__buttons__button', 'reject-button', 'button--secondary',);
                    this.consentBoxButtons.prepend(this.rejectButton);
                    this.setRejectButtonHandler();
                });
            }
            /* ACCEPT BUTTON */
            if(CookiesTexts.hasOwnProperty('acceptButtonLabel')) {
                TemplateRenderer.getTemplate('button-cta')
                .then((template) => {
                    template = template.replace('LABEL', CookiesTexts.acceptButtonLabel);
                    this.acceptButton = TemplateRenderer.createElementFromTemplate(template);
                    this.acceptButton.classList.add( 'data-consent-box__buttons__button', 'accept-button', 'button--cta',);
                    this.consentBoxButtons.appendChild(this.acceptButton);
                    this.setAcceptButtonHandler();
                });
            }
        });

        this.consentBox.appendChild(this.consentBoxButtons);
        
        document.body.appendChild(this.consentBox);
    }

    createConsentsForm(consentsCategories, labelsTextsObject, defaultChecked) {
        const form = document.createElement("form");
        form.method = 'post';
        form.classList.add('data-consent-box__form', 'consent-categories');
        const userInputsEnabled = this.checkIfUserInputsAreEnabled();
        userInputsEnabled ? form.classList.add('form-enabled') : '';
        for (let i = 0; i < consentsCategories.length; i++) {
            
            const inputGroup = document.createElement("div");
            inputGroup.className = 'input-group';
            const inputName = consentsCategories[i] + "-cookies";
            const input = document.createElement("input");
            input.id = inputName;
            input.className = 'cookies-category';
            input.name = inputName;
            input.type = userInputsEnabled ? 'checkbox' : 'hidden';
            input.checked = defaultChecked;
            if (input.name == necessaryStorageName) {
                input.checked = true;
                input.disabled = true;
            }
            
            inputGroup.appendChild(input);

            if(userInputsEnabled) {
                const label = document.createElement("label");
                label.htmlFor = inputName;
                label.innerText = labelsTextsObject.categoriesNames[i];
                label.classList.add('cookies-category__label');
                inputGroup.appendChild(label);
            }
            
            form.appendChild(inputGroup);
        }
        return form;
    }

    checkIfUserInputsAreEnabled() {
        return this.cookiesManagerParams.hasOwnProperty('enable_user_inputs') && this.cookiesManagerParams.enable_user_inputs;
    }

    saveDefaultCookiesConsents() {
        CCR.saveConsentAnalytic(this.cookiesManagerParams.defaults.analytic);
        CCR.saveConsentMarketing(this.cookiesManagerParams.defaults.marketing);
        PrivacyPolicyConsents.autoUpdateCheckboxesOnPrivacyPolicy();
    }

    getTextsForCurrentLanguage() {
        const defaultLanguage = 'pl';
        return CookiesBox.getCurrentLanguage() in this.cookiesManagerParams.texts ? this.cookiesManagerParams.texts[CookiesBox.getCurrentLanguage()] : this.cookiesManagerParams.texts[defaultLanguage];
    }

    static getCurrentLanguage() {
        let langParam = null;

        if( typeof URLSearchParams !== 'function' ) {
            const urlSearchParams = (name) => {
                const results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.search);
                if (results == null){
                    return null;
                }
                else {
                    return decodeURI(results[1]) || 0;
                }
            };

            langParam = urlSearchParams('lang');
        }
        else {
            
            let urlParams = new URLSearchParams(window.location.search);
            langParam = urlParams.get('lang');
        }

        /**
        * konwertuje kod języka do postaci używanej w tablicy cookie tekstów. Przykład konwersji:  pl_PL => pl , pl-PL => pl, pl => pl
        */
        const getLanguageRootFromHTMLLangAttribute = (htmlLangAttribute) => {
            
            const regexPattern = /^(.*?)(?=\-)|^(.*?)(?=\_)|(^.*)/g;
            const lang = htmlLangAttribute.match(regexPattern);

            return lang !== -1 && lang.length > 0 ? lang[0] : null;
        };

        return langParam ? langParam : document.querySelector('html').lang != null ? getLanguageRootFromHTMLLangAttribute(document.querySelector('html').lang) : 'pl';

    }

    onAcceptNoticeHandler(e) {
        e.preventDefault();
        CCR.saveConsentNecessary(true);
        this.closeCookiesBox();
    }

    closeCookiesBox() {
        this.consentBox.parentElement.removeChild(this.consentBox);
    }

    saveConsents(consents) {
        // TODO dodać sprawdzenie czt consents nie są puste i czy mają właściwości
        CCR.saveConsentNecessary(consents.necessary);
        CCR.saveConsentAnalytic(consents.analytic);
        CCR.saveConsentMarketing(consents.marketing);
    }

    acceptCookies(callback) {
        
        this.closeCookiesBox();
        let consents = this.getConsentsToSave();
        if(consents) {
            this.saveConsents(consents);
            PrivacyPolicyConsents.autoUpdateCheckboxesOnPrivacyPolicy();
            if (callback) callback(consents);
        }
    }

    onRejectClickHandler() {
        CCR.saveConsentNecessary(true);
        CCR.saveConsentAnalytic(false);
        CCR.saveConsentMarketing(false);
        this.closeCookiesBox();
    }

    getConsentsToSave() {
        if(this.consentsForm) {
            const analyticConsent = this.consentsForm.querySelector(`#${analyticStorageName}`);
            const marketingConsent = this.consentsForm.querySelector(`#${marketingStorageName}`);
            const consents = { 
                necessary: true, 
                analytic: analyticConsent && analyticConsent.checked ? true : false, 
                marketing: marketingConsent && marketingConsent.checked ? true : false
            };
            return consents;
        }
        return false;
    }
}
