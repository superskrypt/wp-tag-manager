
# WP TAG Manager.


## INSTALACJA

W projekcie:

Do composer.json należy dodać:
```json
    {
        "repositories": [
            {
                "type": "git",
                "url": "git@bitbucket.org:superskrypt/wp-tag-manager"
            },
        "scripts": {
                "post-update-cmd": "composer run-script build -d ./vendor/superskrypt/wp-tag-manager"
            }
        ]
    }
```  
w terminalu:
```
composer require superskrypt/wp-tag-manager
```  

## KONFIGURACJA
  
Konfigurację `wp-tag-manager` należy umieścić w pliku `wp-config.php` w głównym katalogu wordpressa.
Poszczególne parametry konfiguracyjne podaję się poprzez definiowanie określonych stałych php
  
### Parametry konfiguracyjne:
  * `COOKIES_MANAGER_NOTICE` – `true|false` – Określa czy wp-tag-manager powinien działać w trybie `"Consent"` czy `"Notice"`
  * `COOKIES_MANAGER_DISABLE_USER_INPUTS` – `true|false` – Określa czy formularz z checkboxami poszczególnych zgód powinien się wyświetlić. Ustawienie to powiązane jest z `COOKIES_MANAGER_NOTICE=false` oraz z kluczem `categoriesNames` w `COOKIES_MANAGER_TEXTS`
  * `COOKIES_MANAGER_REJECT_MODE` – `true|false` – Określa czy wp-tag-manager będzie umożliwiał użytkownikowi odrzucenie wszystkich zgód. W "consent boksie" renderowany jet dodatkowy guzik.
  * `COOKIES_MANAGER_TEXTS` – `array` -  Teksty lub fragmenty html jakie zostaną dodane do "consent boxa". Wartością stałej `COOKIES_MANAGER_TEXTS` jest tablica asocjacyjna zawierająca klucze, których nazwy zgodne są z kodem językowym np,: pl , en itp. Każdy klucz jest kolejną tablicą assocjacyjną zawierającą poszczególne elementy tekstowe consent boxa. Są to:
     * `headingText` - tekst wyświetlany jako h2 w consent boksie.
     * `content` - treść informacji o zbieraniu danych. Może zawierać markup html oraz shortcode `PRIVACY_LINK`.
     * `afterContent` - treść wyświetlana po treści content.Zanjduje się w tym samym kontenerze w którym jest `content`
     * `acceptButtonLabel` - Tekst guzika akceptującego zbieranie danych.
     * `rejectButtonLabel` - Teks guzika odrzucającego zgodę na zbieranie danych.
     * `categoriesNames` - Tłumaczenia etykiet checkboxów dla poszczególnych zgód (w przypadku gdy wyświetlany jest formularz). Kolejność jest istotna `!!! DO POTWIERDZENIA !!!` - 1: 'necessary', 2: 'analytic', 3.'marketing'
  
### Parametry konfiguracyjne dla usłóg zbierania danych.
Parametry konfiguracyjne Google Analitics:  
 * `GOOGLE_ANALITICS_ID` - ID google analitics dla danej strony

Parametry konfiguracyjne Google Tag Manager:  
 * `ANALYTIC_GTM_CONTAINER_ID` - ID google tag manager dla danej strony

Parametry konfiguracyjne Facebook Pixel:  
 * `FACEBOOK_PIXEL_ID` - IDfacebook pixel dla danej strony
  
Parametry konfiguracyjne Hotjar:  
 * `HOTJAR_SITE_ID` - IDfacebook pixel dla danej strony
Parametry konfiguracyjne Hotjar:  
 * `HOTJAR_SITE_ID` - IDfacebook pixel dla danej strony


**UWAGA! - jeśli w konfiguracji nie ma podanych tłumaczeń do określonego języka oraz nie ma ich również w domyślnych ustawieniach konfiguracyjnych, wówczas teksty pobierane są z pierwszego skonfigurowanego języka**  


Przykładowa konfiguracja dla trybu zgody (consent), czyli pozwalający użytkownikowi wybrać na jakie typy zbierania danych zgadza się.
```php
    define( 'GOOGLE_ANALITICS_ID', 'XX-XXXXXXXX-X' );
    define( 'COOKIES_MANAGER_NOTICE', false); // USTAWIA "TRYB ZGODY"
    define( 'COOKIES_MANAGER_DISABLE_USER_INPUTS', false); // Pozwala na wyświetlenie formularza z checkoboxami poszczególnych zgód (zgody zdefioniowane są w kluczy "categoriesNames")
    define( 'COOKIES_MANAGER_REJECT_MODE', false); // Określa czy powinien być wygenerowany guzik 'Odrzuć'
	define( 'COOKIES_MANAGER_TEXTS', array( 
        'pl' => array(
			'headingText'		=> "<h2>Polityka prywatności</h2>",
			'content' 		    => 'Używamy plików cookies',
			'afterContent' 		=> '[PRIVACY_LINK label="Chcę wiedzieć więcej"]',
			'acceptButtonLabel' => 'Akceptuję',
            'rejectButtonLabel' => 'Odrzuć',
			'categoriesNames'	=> ['koniecznych', 'analitycznych', 'marketingowych'],
		),
		'en' => array(
			'headingText'		=> "<h2>Privacy Policy</h2>",
			'content' 		    => 'We use cookies',
			'afterContent' 		=> '[PRIVACY_LINK label="I want to know more"]',
			'acceptButtonLabel' => 'Accept',
            "rejectButtonLabel" => 'reject',
			'categoriesNames'	=> ['necessary', 'analytic', 'marketing'],
		),
		'ru' => array(
			'headingText'		=> "<h2>политика конфиденциальности</h2>",
			'content' 		    => 'Мы используем файлы «cookie»',
			'acceptButtonLabel' => 'Я принимаю',
			'afterContent' 		=> '[PRIVACY_LINK label="Я хочу узнать больше"]',
            "rejectButtonLabel" => 'Reject',
			'categoriesNames'	=> ['необходимые', 'аналитические', 'маркетинговые'],
		),
	));
```

Przykładowa konfiguracja dla trybu powiadomienia (notice)  

```php
    define( 'GOOGLE_ANALITICS_ID', 'XX-XXXXXXXX-X' );
    define( 'COOKIES_MANAGER_NOTICE', true); 
    define( 'COOKIES_MANAGER_DISABLE_USER_INPUTS', true);
    define( 'COOKIES_MANAGER_REJECT_MODE', true);
	define( 'COOKIES_MANAGER_TEXTS', array( 
		'pl' => array(
			'headingText'		=> "",
			'content' 		    => 'Używamy plików cookies w celu poprawy jakości serwisu. Przetwarzamy jedynie dane zanonimizowane.',
			'afterContent' 		=> '[PRIVACY_LINK label="Chcę wiedzieć więcej"]',
            'acceptButtonLabel' => 'Rozumiem',
		),
		'en' => array(
			'headingText'		=> "",
			'content' 		    => 'We use cookies.',
			'afterContent' 		=> '[PRIVACY_LINK label="I want to know more"]',
			'acceptButtonLabel' => 'I understand',
		),
		'ru' => array(
			'headingText'		=> "",
			'content' 	        => 'Наш веб-сайт использует файлы cookie.',
            'afterContent' 		=> '[PRIVACY_LINK label="Я хочу знать больше"]',
			'acceptButtonLabel' => 'Я понимаю',
		),
    ));
```  

## Edytor Ustawień (na stronie typu "Polityka prywatności")

Wp Tag Manager posiada możliwość wyświetlenia na dowolnej stronie (w założeniu powinna to być strona typu  "Polityka prywatności") kompaktowego edytora dla ustawień zgód użytkownika. Użytownik dzięki temu może w każdej chwili zmienić zgody których udzielił z poziomu Consent Box-a.  
Aby dodać ten edytor należy użyć shortcode `[cookies_settings]`. Domyślnie teksty wyświtlaja się w aktualnym języku. Ustawienie to mozna nadpisać przekazując do shortcode atrybut `lang` np.: `[cookies_settings lang=en]`

### Konfiguracja tekstów edytora ustawień

Aby nadpisać domyślne teksty edytora ustawień, albo dodać teksty dla nowego języka trzeba zdefiniować stałą `CUSTOM_PRIVACY_POLICY_CONSENT_SETTINGS_TEXTS` i do niej przypisać tablicę tekstów, gdzie kluczami będą kody języka - struktura powinno być według poniższego przykładu.

Domyślne teksty:
```php
define('PRIVACY_POLICY_CONSENT_SETTINGS_TEXTS', array(
	'pl' => array(
		'header' 			=> 'Ustawienia ciasteczek w Twojej przeglądarce',
		'categoriesNames'	=> ['niezbędne pliki cookies', 'cookies analityczne', 'cookies marketingowe'],
		'button' 			=> 'Zapisz ustawienia',
	),
	'en' => array(
		'header' 			=> 'Cookies settings in your browser',
		'categoriesNames'	=> ['necessary cookies', 'analytic cookies', 'marketing cookies'],
		'button' 			=> 'Save preferences',
	),
	'ru' => array(
		'header' 			=> 'Мы используем необходимые/аналитические/маркетинговые файлы',
		'categoriesNames'	=> ['necessary cookies', 'analytic cookies', 'marketing cookies'],
		'button' 			=> 'сохранить настройки',
	),
));
```


## DEVELOPMENT

```
git clone https://mac_tur@bitbucket.org/superskrypt/wp-tag-manager.git
``` 
```
composer install
```
```
npm install
```  
```
npm run watch

```  

### Rozwiązywanie problemów z konfiguracją domyślnego środowiska

Jeśli podczas wykonania komend `npm install` lub `npm run watch` pojawi sie kod błędu `code: 'ERR_OSSL_EVP_UNSUPPORTED'` należy przed komendą  `npm install` lub `npm run watch` wpisać  `NODE_OPTIONS=--openssl-legacy-provider`
```console
NODE_OPTIONS=--openssl-legacy-provider npm run watch
```  
